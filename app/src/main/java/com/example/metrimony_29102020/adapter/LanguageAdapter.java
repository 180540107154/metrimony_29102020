package com.example.metrimony_29102020.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.metrimony_29102020.model.LanguageModel;

import java.util.ArrayList;

public class LanguageAdapter extends BaseAdapter {
    Context context ;
    ArrayList<LanguageModel> languagelist;

    public LanguageAdapter(Context context, ArrayList<LanguageModel> languagelist){
        this.context = context;
        this.languagelist = languagelist;
    }


    @Override
    public int getCount() {
        return languagelist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }
}
