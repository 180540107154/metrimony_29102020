package com.example.metrimony_29102020.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.metrimony_29102020.R;
import com.example.metrimony_29102020.model.CityModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CityAdapter extends BaseAdapter {

    Context context;
    ArrayList<CityModel> citylist;

    public CityAdapter(Context context, ArrayList<CityModel> citylist) {
        this.context = context;
        this.citylist = citylist;
    }

    @Override
    public int getCount() {
        return citylist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewgroup) {
        View v = view;
        CityAdapter.ViewHolder viewHolder;
        if (v == null) {
            v = LayoutInflater.from(context).inflate(R.layout.view_dropdown_text,null);
            viewHolder = new ViewHolder(v);
            v.setTag(viewHolder);
        }else {
            viewHolder = (CityAdapter.ViewHolder) v.getTag();
        }
        viewHolder.tvActDisplaySpinnerList.setText(citylist.get(i).getCityName());
        return v;
    }


    static
    class ViewHolder {
        @BindView(R.id.tvActDisplaySpinnerList)
        TextView tvActDisplaySpinnerList;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
