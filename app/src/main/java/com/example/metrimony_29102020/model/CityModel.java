package com.example.metrimony_29102020.model;

import java.io.Serializable;

public class CityModel implements Serializable {

    public int CityId;
    public String CityName;

    public int getCityId(int anInt) {
        return CityId;
    }

    public void setCityId(int cityId) {
        CityId = cityId;
    }

    public String getCityName() {
        return CityName;
    }

    public void setCityName(String cityName) {
        CityName = cityName;
    }

    @Override
    public String toString() {
        return "CityModel{" +
                "CityId=" + CityId +
                ", CityName='" + CityName + '\'' +
                '}';
    }
}
