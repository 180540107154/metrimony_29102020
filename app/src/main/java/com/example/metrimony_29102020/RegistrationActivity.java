package com.example.metrimony_29102020;

import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.metrimony_29102020.adapter.CityAdapter;
import com.example.metrimony_29102020.adapter.LanguageAdapter;
import com.example.metrimony_29102020.database.TblMstCitiy;
import com.example.metrimony_29102020.database.TblMstLanguage;
import com.example.metrimony_29102020.model.CityModel;
import com.example.metrimony_29102020.model.LanguageModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegistrationActivity extends AppCompatActivity {

    @BindView(R.id.etActFirstName)
    EditText etActFirstName;
    @BindView(R.id.etActMiddleName)
    EditText etActMiddleName;
    @BindView(R.id.etActLastName)
    EditText etActLastName;
    @BindView(R.id.etActMobileNumber)
    EditText etActMobileNumber;
    @BindView(R.id.etActEmailAddress)
    EditText etActEmailAddress;
    @BindView(R.id.etActDob)
    EditText etActDob;
    @BindView(R.id.spCity)
    Spinner spCity;
    @BindView(R.id.spLanguage)
    Spinner spLanguage;
    @BindView(R.id.rbMale)
    RadioButton rbMale;
    @BindView(R.id.rbFemale)
    RadioButton rbFemale;
    @BindView(R.id.rgGender)
    RadioGroup rgGender;
    @BindView(R.id.cbActCricket)
    CheckBox cbActCricket;
    @BindView(R.id.cbActFootball)
    CheckBox cbActFootball;
    @BindView(R.id.cbActTennis)
    CheckBox cbActTennis;
    @BindView(R.id.cbActHockey)
    CheckBox cbActHockey;
    @BindView(R.id.btnActSubmit)
    Button btnActSubmit;

    ArrayList<CityModel> citylist = new ArrayList<>();

    CityAdapter cityAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration);
        ButterKnife.bind(this);
        setSpinner();
    }


    void setSpinner(){
        citylist.addAll(new TblMstCitiy(this).getCityList());

        cityAdapter = new CityAdapter(this,citylist);

        spCity.setAdapter(cityAdapter);

    }

    @OnClick(R.id.btnActSubmit)
    public void onClick() {
    }
}
