package com.example.metrimony_29102020;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.cvActRegistration)
    CardView cvActRegistration;
    @BindView(R.id.cvActSearch)
    CardView cvActSearch;
    @BindView(R.id.cvActList)
    CardView cvActList;
    @BindView(R.id.cvActFavourite)
    CardView cvActFavourite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.cvActRegistration, R.id.cvActSearch, R.id.cvActList, R.id.cvActFavourite})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cvActRegistration:
                Intent intent = new Intent(MainActivity.this,RegistrationActivity.class);
                startActivity(intent);
                break;
            case R.id.cvActSearch:
                break;
            case R.id.cvActList:
                break;
            case R.id.cvActFavourite:
                break;
        }
    }
}