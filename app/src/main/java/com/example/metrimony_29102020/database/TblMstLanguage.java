package com.example.metrimony_29102020.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.metrimony_29102020.model.LanguageModel;

import java.util.ArrayList;

public class TblMstLanguage extends MyDatabase {

    public static String TABLE_NAME = "TblMstLanguage";
    public static String LANGUAGE_ID = "LanguageId";
    public static String LANGUAGE_NAME = "LanguageName";

    public TblMstLanguage(Context context) {
        super(context);
    }

    public ArrayList<LanguageModel> getLanguageList(){
        SQLiteDatabase db  = getReadableDatabase();
        ArrayList<LanguageModel> list = new ArrayList<>();
        String query = " SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query,null);
        cursor.moveToFirst();
        for (int i = 0 ; i < cursor.getCount(); i++){
            LanguageModel languageModel = new LanguageModel();
            languageModel.setLanguageId(cursor.getInt(cursor.getColumnIndex(LANGUAGE_ID)));
            languageModel.setLanguageName(cursor.getString(cursor.getColumnIndex(LANGUAGE_NAME)));
            list.add(languageModel);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }
}
