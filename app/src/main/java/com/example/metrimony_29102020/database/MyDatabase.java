package com.example.metrimony_29102020.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class MyDatabase extends SQLiteAssetHelper {

    public static String DATABASE_NAME = "metrimony_29102020.db";
    public static int DATABASE_VERSION = 1;

    public MyDatabase(Context context) {
        super(context,DATABASE_NAME,null, DATABASE_VERSION);
    }
}
