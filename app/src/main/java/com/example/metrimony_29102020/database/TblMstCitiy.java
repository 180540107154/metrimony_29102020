package com.example.metrimony_29102020.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.metrimony_29102020.model.CityModel;

import java.util.ArrayList;

public class TblMstCitiy extends MyDatabase {

    public static String TABLE_NAME = "TblMstCitiy";
    public static String CITY_ID = "CityId";
    public static String CITY_NAME = "CityName";

    public TblMstCitiy(Context context) {
        super(context);
    }


    public ArrayList<CityModel> getCityList(){
        SQLiteDatabase db =getReadableDatabase();
        ArrayList<CityModel> list = new ArrayList<>();
        String query = " SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query,null);
        cursor.moveToFirst();
        for (int i = 0 ; i < cursor.getCount() ; i++){
            CityModel cityModel  = new CityModel();
            cityModel.setCityId(cursor.getInt(cursor.getColumnIndex(CITY_ID)));
            cityModel.setCityName(cursor.getString(cursor.getColumnIndex(CITY_NAME)));
            list.add(cityModel);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }
}
